var gulp = require("gulp");
var ts = require("gulp-typescript");
var shell = require("gulp-shell");
var tsProject = ts.createProject('./tsconfig.json');


gulp.task("default", function() {
    return gulp.src("src/**/*.ts")
        .pipe(tsProject())
        .pipe(gulp.dest('build'));
});

gulp.task("run", ["default"], shell.task(["node build/index.js"]));
