import { ISequelizeConfig } from "sequelize-typescript/lib/interfaces/ISequelizeConfig";
import { Sequelize } from "sequelize";


export class DatabaseConfig implements ISequelizeConfig {

    database: string;

    dialect: string = "postgres";

    username: string = "nany-baby";

    password: string = "nanyBabyUserAdmin";

    operatorsAliases: boolean = false;

    logging: boolean;

    modelPaths: string[];

    constructor (env: string) {
        this.database = (env === "PRODUCTION") ? "nany-baby" : "nany-baby-dev";
    }

    setModelPath (path: string[]) {
        this.modelPaths = path;
    }


}