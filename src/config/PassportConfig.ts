import * as passportFacebook from "passport-facebook";
import * as passportLocal from "passport-local";
import * as passport from "passport";
import { User } from "../entities/User";
import { Request, NextFunction, Response } from "express";
import { PassportStatic } from "passport";
import * as JWT from "jsonwebtoken";
import { AppConfig } from "./AppConfig";
import { Babysitter } from "../entities/Babysitter";
import { Profile } from "../entities/Profile";
import { Parent } from "../entities/Parent";

export class PassportConfig {

    passport: PassportStatic;
    appConfig: AppConfig;

    constructor() {

        this.appConfig = new AppConfig();
        this.passport = passport;
        
        this.passport.serializeUser<any, any>((user, done) => {
            done(undefined, user.id);
        });

        this.passport.deserializeUser(async (id: number, done ) => {
            let user = await User.findById(id);
            if(user) return done(undefined , user);
            return done(undefined);
        });
        
        this.passport.use(new passportFacebook.Strategy({
                clientID: "161575801118293",
                clientSecret: "8ada81356fabec71442894cb52a212d2",
                callbackURL: "/auth/facebook/callback",
                profileFields: ["name", "email", "birthday", "gender", "picture"]
            }, 
            async (accessToken, refreshToken, profile, done) => {
                console.log(profile)
                let userAlreadyCreatedFacebook: User | null = await User.findOne({ where: {facebook: profile.id }, include: [Babysitter, Parent] })
                if(userAlreadyCreatedFacebook) return done(undefined, userAlreadyCreatedFacebook);
                
                let userAlreadyCreatedEmail: User | null = await User.findOne( {where: { email: profile._json.email }});
                if(userAlreadyCreatedEmail) return done(new Error("There is already an account using this email address. Please merge it inside your Account Configurations."));
                
                let newUser: User = new User();
                newUser.email = profile._json.email;
                newUser.facebook = profile.id;
                newUser.isComplete = false;
                newUser = await newUser.save();
                return done(undefined, newUser);
            }
        ));

        this.passport.use(new passportLocal({usernameField: 'email', session: false},
            async function(email, password, done) {
                let user = await User.findOne({where: { email: email }});
                if(user){
                    try {
                        let bcrypt = user.validatePassword(password);
                        if(bcrypt === true) return done(undefined, user);
                        return done(null, false, { message: 'Wrong username or password.' })
                    } catch (err) {
                        return done(err, null);
                    }
                }  else {
                    return done(null, false, { message: 'Wrong username or password.' })
                }

            }
        ));
    }

    isAuthenticated = (req: Request, res: Response, next: NextFunction) => {
        let authHeader: string = <string>req.headers.authorization;
        let token: string = "";

        if(authHeader) token = authHeader.split(' ')[1];
        try {
            if(token) {
                req.user = JWT.verify(token, this.appConfig.JWTSecret);
                return next();
            } else {
                res.send("No token provided").status(403);
            }
        } catch( err) {
            res.send("Invalid token").status(403);
        }
    };
 
}