import { Request, Response, NextFunction } from "express-serve-static-core";
import { Token } from "../models/Token";
import { Babysitter } from "../entities/Babysitter";
import { Parent } from "../entities/Parent";
import { AppConfig } from "../config/AppConfig";


export class AuthController {

    appConfig: AppConfig;

    constructor() {
        this.appConfig = new AppConfig();
    }

    successCallback = (req: Request, res: Response, next: NextFunction): void => {
        let account: Babysitter | Parent = <Babysitter | Parent>req.user;
        let token = new Token(account, this.appConfig.JWTSecret);
        res.send(token.result).status(200);
    };

    successCallbackFacebook = (req: Request, res: Response, next: NextFunction): void => {
        let responseFacebook = req.user;
        if(responseFacebook.isComplete) {
            let _account = responseFacebook.babbysitter ? responseFacebook.babbysitter : responseFacebook.parent;
            console.log(_account);
            let token: Token = new Token(_account, this.appConfig.JWTSecret);
            console.log(token);
            res.redirect('OAuthLogin://login?token=' + token.result );
        } else {
            res.redirect('OAuthLogin://login?user=' + JSON.stringify({email: responseFacebook.email, facebook: responseFacebook.facebook}));
        }
    }

    errorCallback(req: Request, res: Response, next: NextFunction): void {
        res.status(403).send({message: "Wrong username or password"});
    }

}