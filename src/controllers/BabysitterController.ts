import { Request, Response, NextFunction } from "express-serve-static-core";
import { Profile } from "../entities/Profile";
import { Address } from "../entities/Address";
import { Babysitter } from "../entities/Babysitter";
import { Job } from "../entities/Job";
import { PossibleBabysitterJob } from "../entities/PossibleBabysitterJob";
import { Sequelize } from "sequelize-typescript/node_modules/@types/sequelize";

export class BabysitterController {

    sequelize: Sequelize;
    
    constructor(sequelize: Sequelize) {
        this.sequelize = sequelize;
    }

    async update(req: Request, res: Response, next: NextFunction): Promise<void> {
        let babysitterId: number = +req.params.id;
        let updatedProfile: Profile;
        let babysitter = await Babysitter.findById(babysitterId, {include: [{ model: Profile, required: true, include: [Address]}]});

        if (babysitter && req.user.babysitterId === babysitter.id) {
            if(req.body.profile.address){
                await babysitter.profile.address.update(<Address>req.body.profile.address);
            }
            try{
                updatedProfile = await babysitter.profile.update(<Profile>req.body.profile);
                res.status(200).send(updatedProfile);
            }catch (err){
                console.error(err);
                res.status(500).send(err);
            }
        } else if (babysitter) {
            res.status(403).send("Forbidden Access.");
        } else {
            res.status(404).send("Profile not found.");
        }
    }

    setLookingForJob = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        let babysitter: Babysitter = req.user;
        babysitter.isSearching = true;
        if(req.query.mode) babysitter.trafficMode = req.query.mode;
        try {
            let activeJobs: Job[] = await Job.findAll();
            await this.sequelize.transaction( async t => {
                let promisseActiveJobs: PromiseLike<Job>[] = activeJobs.map( job => job.$add('possibleBabysitters', babysitter, {transaction: t}));
                activeJobs = await Promise.all(promisseActiveJobs);
                await babysitter.save({transaction: t});
            });
        } catch (err) {
            console.log(err);
            res.status(500).send("Something went wrong");
        }
        res.status(200).send("OK");
    }

    async isAuthBabysitter(req: Request, res: Response, next: NextFunction): Promise<void> {
        let babysitterId: number = +req.params.id;
        let babysitter = await Babysitter.findById(babysitterId);
        if (babysitter && req.user.babysitterId === babysitter.id) {
            req.user = babysitter;
            next();
        } else if (babysitter) {
            res.status(403).send("Forbidden Access.");
        } else {
            res.status(404).send("Babysitter not found.");
        }
    }
    

}
    