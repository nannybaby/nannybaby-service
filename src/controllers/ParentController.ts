import {Request, Response, NextFunction} from "express-serve-static-core";
import {Profile} from "../entities/Profile";
import {Address} from "../entities/Address";
import {Parent} from "../entities/Parent";
import {Job} from "../entities/Job";
import * as moment from "moment";
import {Child} from "../entities/Child";
import {SpecialCondition} from '../entities/SpecialCondition';
import {Sequelize} from "sequelize-typescript";

export class ParentController {

  sequelize: Sequelize;

  constructor(sequelize: Sequelize) {
    this.sequelize = sequelize;
  }

  async update(req: Request, res: Response, next: NextFunction): Promise<void> {
    let parentId: number = +req.params.id;
    let updatedProfile: Profile;
    let parent = await Parent.findById(parentId, {
      include: [{model: Profile, required: true, include: [Address]}]
    });

    if (parent && req.user.parentId === parent.id) {
      if (req.body.profile.address) {
        await parent.profile.address.update(<Address>req.body.profile.address);
      }
      try {
        updatedProfile = await parent.profile.update(<Profile>req.body.profile);
        res.send(updatedProfile).status(200);
      } catch (err) {
        res.status(500).send(err);
      }
    } else if (parent) {
      res.status(403).send("Forbidden Access.");
    } else {
      res.status(404).send("Profile not found.");
    }
  }

  async setLookingForBabysitter(req: Request, res: Response, next: NextFunction): Promise<void> {
    let parent: Parent = Parent.build(req.user);
    try {
      await Job.create({
        parentId: parent.id,
        time: moment(req.body.time).format("YYYY-MM-DD HH:mm")
      });
    } catch (err) {
      console.log(err);
    }
    res.status(200).send("OK");
  }

  async isAuthParent(req: Request, res: Response, next: NextFunction): Promise<void> {
    let parentId: number = +req.params.id;
    let parent;
    parent = await Parent.findById(parentId);
    if (parent && req.user.parentId === parent.id) {
      req.user = parent;
      next();
    } else if (parent) {
      res.status(403).send("Forbidden Access.");
    } else {
      res.status(404).send("Parent not found.");
    }
  }

  createNewChild = async (req: Request, res: Response): Promise<void> => {
    const _child: Child = <Child>req.body.child;
    let parentId: number = +req.params.id;
    let existentChild = await Child.findOne({
      where: {
        parentId: parentId,
        name: _child.name,
        surname: _child.surname
      }
    });
    if (existentChild) {
      res.status(409).send("Child already created");
      return;
    }
    try {
      let child: Child = new Child(_child);
      child.parentId = parentId;
      await child.validate();
      await this.sequelize.transaction(async t => {
        await child.save({transaction: t});
        await child.$set('specialConditions', _child.specialConditions, {transaction: t});
      });
      res.status(200).send("OK");
    } catch (err) {
      res.status(400).send(err);
    }
  };

  updateChild = async (req: Request, res: Response): Promise<void> => {
    const _child: Child = <Child>req.body.child;
    let childId: number = req.body.id;
    let child = await Child.findById(childId, {include: [SpecialCondition]});
    if (!child) {
      res.status(400).send("Child not found");
      return;
    }
    try {
      await this.sequelize.transaction(async t => {
        if(_child.specialConditions){
          await child.$set('specialConditions', _child.specialConditions, {transaction: t});
          delete _child.specialConditions;
        }
        let updatedChild = await child.update(_child, {transaction: t});
        res.status(200).send(updatedChild);
      });
    } catch (err) {
      res.status(400).send(err);
    }
  }
}
