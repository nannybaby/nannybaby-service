import { Request, Response, NextFunction } from "express-serve-static-core";
import {User} from "../entities/User";
import * as JWT from "jsonwebtoken";
import { Babysitter } from "../entities/Babysitter";
import { Parent } from "../entities/Parent";
import { Profile } from "../entities/Profile";
import { Token } from "../models/Token";
import { AppConfig } from "../config/AppConfig";
import { Address } from "../entities/Address";
import {MailerService} from "../services/MailerService";



export class UserController {

    appConfig: AppConfig;
    mailerService: MailerService;
    
    constructor() {
        this.mailerService = new MailerService();
        this.appConfig = new AppConfig();
    }

    async getOne(req: Request, res: Response, next: NextFunction): Promise<void>{
        let userId: number = +req.params.id;
        let user = await User.findById(userId);
        
        if(user) {
            res.send(user).status(200);  
        } else {
            res.send("User not found").status(404);
        }
    }

    async getMe(req: Request, res: Response, next: NextFunction): Promise<void>{
        let user: Babysitter | Parent | null = req.user.babysitterId ? await Babysitter.findById(req.user.babysitterId, {include: [Profile]}) : await Parent.findById(req.user.parentId, {include: [Profile]});
        if(user) {
            res.send({user: user, type: user.getClass()}).status(200);
        } else {
            res.send("User not found").status(404);
        }
    }
    
    create =  async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        
        const _account: Babysitter | Parent = <Babysitter|Parent>req.body;
        let token: Token;
        let user = await User.findOne({where: {email: _account.user.email}});
        let account: Babysitter | Parent;
        _account.profile.address = <Address>{};

        if(user) {
            res.status(409).send("User already created");
        } else {
            account = req.body.accountType === "babysitter" ?  new Babysitter(_account, {include: [User, {model: Profile, include: [Address]}]}) : new Parent(_account, { include: [User, {model: Profile, include: [Address]}]});
            account.user.confirmedEmail = false;
            account.user.confirmationEmailToken = JWT.sign({userId: account.userId, timestamp: Date.now()}, "CONFIRMATION EMAIL TOKEN", {expiresIn: "1h"});
            try {
                await account.profile.validate();
                await account.user.validate();
                account = await account.save();
                // Mailer Service Working this.mailerService.sendEmailConfirmation(account.user);
            } catch (err) {
                if(err.errors){
                    err = err.errors.map( error => error.message)
                    res.status(400).send(err);
                } else {
                    res.status(500).send(err.message);
                }
                return;
            }
            token = new Token(account, this.appConfig.JWTSecret);
            res.send(token.result);
        }
    }

     completeFacebookUser =  async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        
        const _account: Babysitter | Parent = <Babysitter|Parent>req.body;
        let token: Token;
        let user: User | null = await User.findOne({where: {email: _account.user.email, facebook: _account.user.facebook, isComplete: false}});
        let account: Babysitter | Parent;

        if(!user) {
            res.status(404).send("User not found or already created");
        } else {
            account = req.body.accountType === "babysitter" ?  new Babysitter(_account, {include: [{model: Profile, include: [Address]}]}) : new Parent(_account, { include: [{model: Profile, include: [Address]}]});
            user.password = _account.user.password;
            user.confirmedEmail = false;
            user.confirmationEmailToken = JWT.sign({userId: account.userId, timestamp: Date.now()}, "CONFIRMATION EMAIL TOKEN", {expiresIn: "1h"});
            user.isComplete = true;
            try {
                await account.profile.validate();
                user = await  user.save();
                account.userId = user.id;
                account = await account.save();
                // Mailer Service Working this.mailerService.sendEmailConfirmation(account.user);
            } catch (err) {
                if(err.errors){
                    err = err.errors.map( error => error.message)
                    res.status(400).send(err);
                } else {
                    res.status(500).send(err.message);
                }
                return;
            }
            token = new Token(account, this.appConfig.JWTSecret);
            res.send(token.result);
        }
    }
    
    async confirmEmail(req: Request, res: Response, next: NextFunction): Promise<void>{
        
        const token: string = req.query.token;
        const verifiedToken: string | object  = JWT.verify(token, "CONFIRMATION EMAIL TOKEN");
        let user: User | null = null;

        if(verifiedToken) user = await User.findOne({where: {confirmedEmail: false, confirmationEmailToken: token}});
        if(user) {
            user.confirmedEmail = true;
            user = await user.save();
            res.send("E-mail confirmed.").status(200);
        } else {
            res.send("E-mail already confirmed or invalid token.").status(403);
        }
    }
}

// TODO Password Recovery Method (maybe goes to AuthController)

// async recoverPassword(req: Request, res: Response, next: NextFunction): Promise<void>{
//     let email = req.body.email;
//     let user = await User.findOne({where: {email}});
//     if(user) {
//         user.passwordRecoverToken = JWT.sign({user: user.id, timestamp: Date.now().toString()}, "EMAIL VERIFICATION SUPER SECRET", { expiresIn: '1h' })
//         user.save();
//         // TODO Create a service for sending an e-mail with the token
//         res.status(200);
//     } else {
//         res.send("User not found").status(404);
//     }
// }

// async recoverPasswordCallback(req: Request, res: Response, next: NextFunction): Promise<void>{
//     let token = req.query.token;
//     let user = await User.findOne({where: {passwordRecoverToken: token}});
//     if(user && user.verifyRecoverToken(token)) {
//         user.password = req.body.password;
//         user.save();
//         // TODO Create a service for sending an e-mail with the token
//         res.status(200);
//     } else {
//         res.send("Invalid token").status(404);
//     }
// }