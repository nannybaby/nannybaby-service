import { Model } from "sequelize-typescript/lib/models/Model";
import { PrimaryKey } from "sequelize-typescript/lib/annotations/PrimaryKey";
import { Column } from "sequelize-typescript/lib/annotations/Column";
import { DataType } from "sequelize-typescript/lib/enums/DataType";
import { Table } from "sequelize-typescript/lib/annotations/Table";

@Table
export class Address extends Model<Address> {

    /**
     * @type {string}
     */
    @Column
    postalCode: string;

    /**
     * @type {string}
     */
    @Column
    street: string;

    /**
     * @type {string}
     */
    @Column
    number: string;

    /**
     * @type {string}
     */
    @Column(DataType.TEXT)
    neighborhood: string;

    /**
     * @type {string}
     */
    @Column
    city: string;

    /**
     * @type {string}
     */
    @Column
    country: string;

    /**
     * @type {string}
     */
    @Column
    state: string;

    /**
     * @type {string}
     */
    @Column
    complement: string;

}
