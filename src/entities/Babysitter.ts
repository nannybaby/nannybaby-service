import {Profile} from "./Profile";
import { User } from "./User";
import { Model } from "sequelize-typescript/lib/models/Model";
import { Column } from "sequelize-typescript/lib/annotations/Column";
import { BelongsTo } from "sequelize-typescript/lib/annotations/association/BelongsTo";
import { ForeignKey } from "sequelize-typescript/lib/annotations/ForeignKey";
import { Table } from "sequelize-typescript/lib/annotations/Table";
import { BelongsToMany } from "sequelize-typescript/lib/annotations/association/BelongsToMany";
import { SpecialCondition } from "./SpecialCondition";
import { BabysitterSpecialCondition } from "./BabysitterSpecialCondition";
import { Job } from "./Job";
import { HasMany } from "sequelize-typescript/lib/annotations/association/HasMany";
import { PossibleBabysitterJob } from "./PossibleBabysitterJob";

@Table
export class Babysitter extends Model<Babysitter> {

    /**
     * @type {Profile}
     */
    @BelongsTo( () => Profile, 'profileId')
    profile: Profile;

    /**
     * @type {number}
     */
    @ForeignKey(() => Profile)
    @Column
    profileId: number;

    /**
     * @type {User}
     */
    @BelongsTo( () => User, 'userId')
    user: User;

    /**
     * @type {number}
     */
    @ForeignKey(() => User)
    @Column
    userId: number;

    @Column
    maxChildren: number;

    @Column
    isSearching: boolean;

    @Column
    trafficMode: string;

    @BelongsToMany(() => SpecialCondition, () => BabysitterSpecialCondition)
    specialConditions: SpecialCondition[];

    @BelongsToMany(() => Job, () => PossibleBabysitterJob)
    possibleJobs: Job[];

    @HasMany(() => Job)
    jobs: Job[];


    getClass() {
        return this.constructor.name;
    }

}
