import { Model } from "sequelize-typescript/lib/models/Model";
import { Table } from "sequelize-typescript/lib/annotations/Table";
import { ForeignKey } from "sequelize-typescript/lib/annotations/ForeignKey";
import { Column } from "sequelize-typescript/lib/annotations/Column";
import { SpecialCondition } from "./SpecialCondition";
import { Babysitter } from "./Babysitter";

@Table
export class BabysitterSpecialCondition extends Model<BabysitterSpecialCondition> {

  @ForeignKey(() => Babysitter)
  @Column
  babysitterId: number;

  @ForeignKey(() => SpecialCondition)
  @Column
  specialConditionId: number;

}