import {Parent} from "./Parent";
import {BelongsTo} from "sequelize-typescript/lib/annotations/association/BelongsTo";
import {Table} from "sequelize-typescript/lib/annotations/Table";
import {Model} from "sequelize-typescript/lib/models/Model";
import {PrimaryKey} from "sequelize-typescript/lib/annotations/PrimaryKey";
import {Column} from "sequelize-typescript/lib/annotations/Column";
import {ForeignKey} from "sequelize-typescript/lib/annotations/ForeignKey";
import {HasMany} from "sequelize-typescript/lib/annotations/association/HasMany";
import {SpecialCondition} from "./SpecialCondition";
import {ChildSpecialCondition} from "./ChildSpecialCondition";
import {BelongsToMany} from "sequelize-typescript/lib/annotations/association/BelongsToMany";
import {ChildJob} from "./ChildJob";
import {Job} from "./Job";
import {DataType, Default} from "sequelize-typescript";

@Table
export class Child extends Model<Child> {

  /**
   * @type {Parent}
   */
  @BelongsTo(() => Parent, 'parentId')
  parent: Parent;

  /**
   * @type {number}
   */
  @ForeignKey(() => Parent)
  @Column
  parentId: number;

  @BelongsToMany(() => SpecialCondition, () => ChildSpecialCondition)
  specialConditions: SpecialCondition[];

  @BelongsToMany(() => Job, () => ChildJob)
  jobs: Job[];

  @Column
  name: string;

  @Column
  surname: string;

  // @Column({type: DataType.ENUM('0-3' | '4-5' | '6-9' | '10-18')})
  // state: string;

}
