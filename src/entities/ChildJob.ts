import { Model } from "sequelize-typescript/lib/models/Model";
import { Table } from "sequelize-typescript/lib/annotations/Table";
import { ForeignKey } from "sequelize-typescript/lib/annotations/ForeignKey";
import { Column } from "sequelize-typescript/lib/annotations/Column";
import { Job } from "./Job";
import { Child } from "./Child";

@Table
export class ChildJob extends Model<ChildJob> {

  @ForeignKey(() => Child)
  @Column
  childId: number;

  @ForeignKey(() => Job)
  @Column
  jobId: number;

}