import { Model } from "sequelize-typescript/lib/models/Model";
import { Table } from "sequelize-typescript/lib/annotations/Table";
import { ForeignKey } from "sequelize-typescript/lib/annotations/ForeignKey";
import { Child } from "./Child";
import { Column } from "sequelize-typescript/lib/annotations/Column";
import { SpecialCondition } from "./SpecialCondition";

@Table
export class ChildSpecialCondition extends Model<ChildSpecialCondition> {

  @ForeignKey(() => Child)
  @Column
  childId: number;

  @ForeignKey(() => SpecialCondition)
  @Column
  specialConditionId: number;

}