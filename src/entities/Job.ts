import {Parent} from "./Parent";
import { Babysitter } from "./Babysitter";
import { Model } from "sequelize-typescript/lib/models/Model";
import { BelongsTo } from "sequelize-typescript/lib/annotations/association/BelongsTo";
import { ForeignKey } from "sequelize-typescript/lib/annotations/ForeignKey";
import { Table } from "sequelize-typescript/lib/annotations/Table";
import { Column } from "sequelize-typescript/lib/annotations/Column";
import { Child } from "./Child";
import { BelongsToMany } from "sequelize-typescript/lib/annotations/association/BelongsToMany";
import { PossibleBabysitterJob } from "./PossibleBabysitterJob";
import { ChildJob } from "./ChildJob";
import { DataType } from "sequelize-typescript/lib/enums/DataType";

@Table
export class Job extends Model<Job> {

    /**
     * @type {Parent}
     */
    @BelongsTo( () => Parent)
    parent: Parent;

    /**
     * @type {number}
     */
    @ForeignKey(() => Parent)
    @Column
    parentId: number;

    /**
     * @type {Babysitter}
     */
    @BelongsTo( () => Babysitter)
    babysitter: Babysitter;

    /**
     * @type {number}
     */
    @ForeignKey(() => Babysitter)
    @Column
    babysitterId: number

    @BelongsToMany(() => Child, () => ChildJob, 'jobId', 'childId')
    children: Child[];

    @BelongsToMany(() => Babysitter, () => PossibleBabysitterJob, 'jobId', 'babysitterId')
    possibleBabysitters: Babysitter[];

    @Column({type: DataType.DATE})
    time: Date

}
