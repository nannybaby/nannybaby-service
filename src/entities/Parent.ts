import {Child} from "./Child";
import {Profile} from "./Profile";
import {User} from "./User";
import { Model } from "sequelize-typescript/lib/models/Model";
import { Table } from "sequelize-typescript/lib/annotations/Table";
import { Column } from "sequelize-typescript/lib/annotations/Column";
import { BelongsTo } from "sequelize-typescript/lib/annotations/association/BelongsTo";
import { ForeignKey } from "sequelize-typescript/lib/annotations/ForeignKey";
import { HasMany } from "sequelize-typescript/lib/annotations/association/HasMany";


@Table
export class Parent extends Model<Parent> {

    /**
     * @type {Profile}
     */
    @BelongsTo( () => Profile, 'profileId')
    profile: Profile;

    /**
     * @type {number}
     */
    @ForeignKey(() => Profile)
    @Column
    profileId: number;

    /**
     * @type {User}
     */
    @BelongsTo( () => User, 'userId')
    user: User;

    /**
     * @type {number}
     */
    @ForeignKey(() => User)
    @Column
    userId: number;

    /**
     * @type {Child[]}
     */
    @HasMany( () => Child)
    child: Child[];

    getClass() {
        return this.constructor.name;
    }

}
