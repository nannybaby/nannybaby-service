import { Model } from "sequelize-typescript/lib/models/Model";
import { Table } from "sequelize-typescript/lib/annotations/Table";
import { ForeignKey } from "sequelize-typescript/lib/annotations/ForeignKey";
import { Column } from "sequelize-typescript/lib/annotations/Column";
import { Babysitter } from "./Babysitter";
import { Job } from "./Job";

@Table
export class PossibleBabysitterJob extends Model<PossibleBabysitterJob> {

    @ForeignKey(() => Babysitter)
    @Column
    babysitterId: number;

    @ForeignKey(() => Job)
    @Column
    jobId: number;

}