import { Address } from "./Address";
import { Babysitter } from "./Babysitter";
import { Parent } from "./Parent";
import { Model } from "sequelize-typescript/lib/models/Model";
import { Table } from "sequelize-typescript/lib/annotations/Table";
import { Column } from "sequelize-typescript/lib/annotations/Column";
import { HasOne } from "sequelize-typescript/lib/annotations/association/HasOne";
import { BelongsTo } from "sequelize-typescript/lib/annotations/association/BelongsTo";
import { ForeignKey } from "sequelize-typescript/lib/annotations/ForeignKey";
import { DataType } from "sequelize-typescript/lib/enums/DataType";
import { IsDate } from "sequelize-typescript/lib/annotations/validation/IsDate";
import { IsBefore } from "sequelize-typescript/lib/annotations/validation/IsBefore";
import * as moment from "moment";


@Table
export class Profile extends Model<Profile> {

    @Column
    bio: string;

    @Column
    phone: string;

    @Column
    name: string;

    @Column
    surname: string;

    @IsDate
    @IsBefore(moment().subtract(18, 'years').add(1, 'days').format("YYYY-MM-DD"))
    @Column({type: DataType.DATEONLY})
    birthDay: Date;

    @Column({type: DataType.GEOMETRY('POINT')})
    point: string;

    @Column
    photo: string;	

    @HasOne( () => Parent, 'profileId')
    parent: Parent;

    @HasOne( () => Babysitter, 'profileId')
    babysitter: Babysitter;

    @BelongsTo( () => Address)
    address: Address;

    @ForeignKey(() => Address)
    @Column
    addressId: number;

}
