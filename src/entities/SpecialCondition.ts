import {Parent} from "./Parent";
import { BelongsTo } from "sequelize-typescript/lib/annotations/association/BelongsTo";
import { Table } from "sequelize-typescript/lib/annotations/Table";
import { Model } from "sequelize-typescript/lib/models/Model";
import { PrimaryKey } from "sequelize-typescript/lib/annotations/PrimaryKey";
import { Column } from "sequelize-typescript/lib/annotations/Column";
import { ForeignKey } from "sequelize-typescript/lib/annotations/ForeignKey";
import { Child } from "./Child";
import { BelongsToMany } from "sequelize-typescript/lib/annotations/association/BelongsToMany";
import { ChildSpecialCondition } from "./ChildSpecialCondition";
import { Babysitter } from "./Babysitter";
import { BabysitterSpecialCondition } from "./BabysitterSpecialCondition";

@Table
export class SpecialCondition extends Model<SpecialCondition> {
    
    @Column
    name: string;

    @BelongsToMany(() => Child, () => ChildSpecialCondition)
    children: Child[];

    @BelongsToMany(() => Babysitter, () => BabysitterSpecialCondition)
    babysitter: Babysitter[];

}
