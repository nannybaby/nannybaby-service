import * as Bcrypt from "bcrypt";
import { Parent } from "./Parent";
import { Babysitter } from "./Babysitter";
import { Table } from "sequelize-typescript/lib/annotations/Table";
import { Model } from "sequelize-typescript/lib/models/Model";
import { Column } from "sequelize-typescript/lib/annotations/Column";
import { DataType } from "sequelize-typescript/lib/enums/DataType";
import { DeletedAt } from "sequelize-typescript/lib/annotations/DeletedAt";
import { CreatedAt } from "sequelize-typescript/lib/annotations/CreatedAt";
import { UpdatedAt } from "sequelize-typescript/lib/annotations/UpdatedAt";
import { BeforeCreate } from "sequelize-typescript/lib/annotations/hooks/BeforeCreate";
import { HasOne } from "sequelize-typescript/lib/annotations/association/HasOne";
import { IsEmail } from "sequelize-typescript/lib/annotations/validation/IsEmail";
import { Length } from "sequelize-typescript/lib/annotations/validation/Length";
import { Scopes } from "sequelize-typescript/lib/annotations/Scopes";
import { BeforeUpdate } from "sequelize-typescript/lib/annotations/hooks/BeforeUpdate";
import {Unique} from "sequelize-typescript";
import { Default } from "sequelize-typescript/lib/annotations/Default";

@Scopes({
    parentAndBabysitter: {
      include: [ () => Parent, () => Babysitter]
    }
  })
@Table
export class User extends Model<User> {

    /**
     * @type {string}
     */
    @Unique
    @IsEmail
    @Column({type: DataType.TEXT})
    email: string;

    /**
     * @type {string}
     */
    @Column({type: DataType.TEXT})
    confirmationEmailToken: string

    /**
     * @type {boolean}
     */
    @Column({type: DataType.BOOLEAN})
    confirmedEmail: boolean

    /**
     * @type {string}
     */
    @Length({min: 6})
    @Column({type: DataType.TEXT})
    password: string;

    /**
     * @type {string}
     */
    @Column({type: DataType.TEXT})
    facebook: string;

    /**
     * @type {string}
     */
    @Default(true)
    @Column({type: DataType.BOOLEAN})
    isComplete: boolean;

    /**
     * @type {string}
     */
    @Column({type: DataType.TEXT})
    passwordRecoverToken: string;

    /**
     * @type {Parent}
     */
    @HasOne( () => Parent, 'userId')
    parent: Parent;

    /**
     * @type {Babysitter}
     */
    @HasOne(() => Babysitter, 'userId')
    babbysitter: Babysitter;

    /**
     * @type {Date}
     */
    @CreatedAt
    createAt: Date;

    /**
     * @type {Date}
     */
    @UpdatedAt
    updateAt: Date;

    /**
     * @type {Date}
     */
    @DeletedAt
    deletedAt: Date;

    @BeforeUpdate
    @BeforeCreate
    static hashPassword(instance: User): void {
        if(instance.changed('password')) instance.password = Bcrypt.hashSync(instance.password, 10);
    }

    validatePassword(plainTextPassword: string): boolean {
		return Bcrypt.compareSync(plainTextPassword, this.password);
    }

    
    
}


