import "reflect-metadata";
import * as express from "express";
import {Express} from "@Types/express-serve-static-core";
import * as bodyParser from "body-parser";
import {PassportConfig} from "./config/PassportConfig";
import {DatabaseConfig} from "./config/DatabaseConfig";
import { AuthRouter } from "./routes/AuthRouter";
import { ParentRouter } from "./routes/ParentRouter";
import { BabysitterRouter } from "./routes/BabysitterRouter";
import { UserRouter } from "./routes/UserRouter";
import {Sequelize} from "sequelize-typescript";

const env = process.env.NODE_ENV;
const app = express();

const passportConfig = new PassportConfig();
const databaseConfig = new DatabaseConfig(env);

// Loading Entities directory
databaseConfig.setModelPath([__dirname + '/entities']);
const sequelize =  new Sequelize(databaseConfig);

const authRouter = new AuthRouter(app);
const babysitterRouter = new BabysitterRouter(app, sequelize);
const parentRouter = new ParentRouter(app, sequelize);
const userRouter = new UserRouter(app, sequelize);

// Connecting to database
sequelize.sync({force: true})
    .then( () => {

        // Setting up app middlewares
        app.set("port", process.env.PORT || 3000);
        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({ extended: true }));
        app.use(passportConfig.passport.initialize());

        // Seting up routes
        authRouter.setRoutes(passportConfig);
        userRouter.setRoutes(passportConfig);
        parentRouter.setRoutes(passportConfig);
        babysitterRouter.setRoutes(passportConfig);

        //Starting to listen on port
        app.listen(app.get('port'), ()=> {
            console.log(("App is running at http://localhost:%d in %s mode"), app.get("port"), app.get("env"));
        });
    })
    .catch( () => {
        console.log("Error starting Sequelize");
        process.exit();
    });
    