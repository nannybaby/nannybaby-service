import { Babysitter } from "../entities/Babysitter";
import { Parent } from "../entities/Parent";
import * as JWT from "jsonwebtoken";

export class Token {

    userId: number;
    
    profileId: number;

    babysitterId: number | null;

    parentId: number | null;
    
    result: string;

    constructor(account: Babysitter | Parent, secret: string) {
        this.userId = account.userId;
        this.profileId = account.profileId;
        this.babysitterId = account.constructor.name === 'Babysitter' ? account.id : null;
        this.parentId = account.constructor.name === 'Parent' ? account.id : null;
        this.result = JWT.sign(JSON.stringify(this), secret);
    }

}
