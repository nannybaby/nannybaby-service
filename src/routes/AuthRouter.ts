import { Application } from "express-serve-static-core";
import {AuthController} from "../controllers/AuthController"
import { PassportConfig } from "../config/PassportConfig";

export class AuthRouter {

    public app: Application;
    public authController: AuthController;

    constructor(app: Application) {
        
        this.app = app;
        this.authController = new AuthController();
    }

    setRoutes(passport: PassportConfig){



        // Facebook Authentication
        this.app.get("/auth/facebook", passport.passport.authenticate("facebook", { scope: ["email", "public_profile"] }));
        this.app.get("/auth/facebook/callback", passport.passport.authenticate("facebook", { failureRedirect: "/auth/error" }), this.authController.successCallbackFacebook);
       
        // Handle error
        this.app.get("/auth/error", this.authController.errorCallback);
        
        // Local Authentication
        this.app.post("/auth/", passport.passport.authenticate("local", { failureRedirect: '/auth/error' }), this.authController.successCallback);
       


    }

}
    