import { Application } from "express-serve-static-core";
import { PassportConfig } from "../config/PassportConfig";
import { BabysitterController } from "../controllers/BabysitterController";
import {MapsService} from "../services/MapsService"
import { Sequelize } from "sequelize";

export class BabysitterRouter {

    public app: Application;
    public babysitterController: BabysitterController;


    constructor(app: Application, sequelize: Sequelize) {
        this.app = app;
        this.babysitterController = new BabysitterController(sequelize);

    }

    setRoutes(passport: PassportConfig){

        this.app.get("/babysitter/:id/activate", passport.isAuthenticated, this.babysitterController.isAuthBabysitter, this.babysitterController.setLookingForJob);


        this.app.put("/babysitter/:id", passport.isAuthenticated , this.babysitterController.update);
    }
    
}