import { Application } from "express-serve-static-core";
import { PassportConfig } from "../config/PassportConfig";
import { ParentController } from "../controllers/ParentController";
import {Sequelize} from "sequelize-typescript";

export class ParentRouter {

    public app: Application;
    public parentController: ParentController;

    constructor(app: Application, sequelize: Sequelize) {
        this.app = app;
        this.parentController = new ParentController(sequelize);
    }

    setRoutes(passport: PassportConfig){
        this.app.post("/parent/:id/activate", passport.isAuthenticated, this.parentController.isAuthParent , this.parentController.setLookingForBabysitter);

        this.app.put("/parent/:id", passport.isAuthenticated , this.parentController.update);
        this.app.put("/child/:id", passport.isAuthenticated , this.parentController.updateChild);
        this.app.post("/child/:id", passport.isAuthenticated , this.parentController.createNewChild);
    }
    
}