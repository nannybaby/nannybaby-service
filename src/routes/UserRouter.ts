import { Application } from "express-serve-static-core";
import { PassportStatic } from "passport";
import {UserController} from "../controllers/UserController"
import { PassportConfig } from "../config/PassportConfig";

export class UserRouter {

    public app: Application;
    public userController: UserController;

    constructor(app: Application) {
        this.app = app;
        this.userController = new UserController();
    }

    setRoutes(passport: PassportConfig){
        this.app.get("/user/:id", passport.isAuthenticated , this.userController.getOne);
        this.app.get("/me", passport.isAuthenticated , this.userController.getMe);
        this.app.post("/user", this.userController.create);
        this.app.put("/user", this.userController.completeFacebookUser);
        this.app.get("/confirmEmail", this.userController.confirmEmail);

    }
    
}