import * as NodeMailer from 'nodemailer'
import { Transport } from 'nodemailer';
import { MailOptions } from 'nodemailer/lib/ses-transport';
import { User } from '../entities/User';


export class MailerService {
    
    transporter: any = NodeMailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'nannybabynoreply@gmail.com',
            pass: 'noreplynanny'
        }
     });

     accountConfirmationOptions: MailOptions = {
        from: '"Nanny Baby -  👶" <nannybabynoreply@gmail.com>', // sender address
        subject: 'Confirm your Account ✔', // Subject line
        text: 'Please confirm your account clicking in this link: http://localhost:3000/confirmEmail?token=', // plain text body
    };

    constructor() {

    }

    sendEmailConfirmation (user: User) {

        let mailOptions = this.accountConfirmationOptions;
        mailOptions.to = user.email;
        mailOptions.text += user.confirmationEmailToken;

        this.transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
        });

    }

    

}
    