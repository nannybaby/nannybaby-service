import * as mocha from 'mocha';
import * as chai from 'chai';
const chaiHttp = require('chai-http');
import * as app from '../src/index';
import * as JWT from "jsonwebtoken";
const Browser = require("zombie");


chai.use(chaiHttp);
const expect = chai.expect;

before( function (done) {  
    setTimeout(()=> done(), 1900)
});

describe('authRoutes', () => {

    it('Should create a Babysitter User and Return a Token', () => {
        
        return chai.request(app).post('/user').send({
            "user": {
                "email": "babysiter@test.com.br",
                "password": "testPW"
            },
            "profile": {
                "name": "Babysiter",
                "surname": "Test",
                "birthDay": "1995-01-30"
            },
            "accountType": "babysitter"
        })
        .then(res => {
            expect(res.status).to.eql(200);
            expect(JWT.decode(res.text)).to.eql({userId: 1, profileId: 1, babysitterId: 1, parentId: null});
        });
    });

    it('Should create a Parent User and Return a Token', () => {
        
        return chai.request(app).post('/user').send({
            "user": {
                "email": "parent@test.com.br",
                "password": "testPW"
            },
            "profile": {
                "name": "Parent",
                "surname": "Test",
                "birthDay": "1995-01-30"
            },
            "accountType": "parent"
        })
        .then(res => {
            expect(res.status).to.eql(200);
            expect(JWT.decode(res.text)).to.eql({userId: 2, profileId: 2, babysitterId: null, parentId: 1});
        });
    });

    it('Should login as Babysitter and Return a Token', () => {
        
        return chai.request(app).post('/auth').send({
            "user": {
                "email": "babysitter@test.com.br",
                "password": "testPW"
            }
        })
        .then(res => {
            expect(res.status).to.eql(200);
            expect(JWT.decode(res.text)).to.eql({userId: 1, profileId: 1, babysitterId: 1, parentId: null});
        });
    });

    it('Should login as Babysitter Return a Token', () => {
        
        return chai.request(app).post('/auth').send({
            "user": {
                "email": "parent@test.com.br",
                "password": "testPW"
            }
        })
        .then(res => {
            expect(res.status).to.eql(200);
            expect(JWT.decode(res.text)).to.eql({userId: 2, profileId: 2, babysitterId: null, parentId: 1});
        });
    });

    // // it("Should login with facebook and Create acount with it",function (done) {

    // //     Browser.visit('http://127.0.0.1:3000/auth/facebook',function (err,brw) {

    // //         if(err){
    // //             throw err;
    // //         }
    
    // //         brw.fill('email','ricardo@maisonave.com.br').fill('pass', 'rioos656833')
    // //             .pressButton('login', function (err,brow) {
    // //                 brw.assert.success();
    // //                 done();
    // //             });
    // //     });
    // });

});
